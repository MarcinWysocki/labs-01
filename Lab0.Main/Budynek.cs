﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    class Budynek
    {
        private int x;

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        public virtual string Wirtualna()
        {
            return "Budynek";
        }
    }
}
