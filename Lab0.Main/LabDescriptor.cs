﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class LabDescriptor
    {
        public static Type A = typeof(Budynek);
        public static Type B = typeof(Dom);
        public static Type C = typeof(Mieszkanie);

        public static string commonMethodName = "Metoda";
    }
}
