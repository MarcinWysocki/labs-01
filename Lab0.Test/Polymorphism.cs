﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using h = PK.Test.Helpers;

using Lab0.Main;

namespace Lab0.Test
{
    [TestFixture]
    [TestClass]
    public class Polymorphism
    {
        [Test]
        [TestMethod]
        public void A_Should_Have_Common_Method()
        {
            h.Should_Have_Method(LabDescriptor.A, LabDescriptor.commonMethodName);
        }

        [Test]
        [TestMethod]
        public void B_Should_Have_Common_Method()
        {
            h.Should_Have_Method(LabDescriptor.B, LabDescriptor.commonMethodName);
        }

        [Test]
        [TestMethod]
        public void C_Should_Have_Common_Method()
        {
            h.Should_Have_Method(LabDescriptor.C, LabDescriptor.commonMethodName);
        }

        [Test]
        [TestMethod]
        public void B_Should_Have_Common_Method_With_Different_Result_Than_A()
        {
            // Arrange
            var a = Activator.CreateInstance(LabDescriptor.A);
            var b = Activator.CreateInstance(LabDescriptor.B);

            // Act
            var ra = LabDescriptor.A.GetMethod(LabDescriptor.commonMethodName).Invoke(a, new object[] { });
            var rb = LabDescriptor.A.GetMethod(LabDescriptor.commonMethodName).Invoke(b, new object[] { });

            // Assert
            Assert.That(rb, Is.Not.EqualTo(ra));
        }

        [Test]
        [TestMethod]
        public void C_Should_Have_Common_Method_With_Different_Result_Than_A()
        {
            // Arrange
            var a = Activator.CreateInstance(LabDescriptor.A);
            var c = Activator.CreateInstance(LabDescriptor.C);

            // Act
            var ra = LabDescriptor.A.GetMethod(LabDescriptor.commonMethodName).Invoke(a, new object[] { });
            var rc = LabDescriptor.A.GetMethod(LabDescriptor.commonMethodName).Invoke(c, new object[] { });

            // Assert
            Assert.That(rc, Is.Not.EqualTo(ra));
        }

        [Test]
        [TestMethod]
        public void C_Should_Have_Common_Method_With_Different_Result_Than_B()
        {
            // Arrange
            var b = Activator.CreateInstance(LabDescriptor.B);
            var c = Activator.CreateInstance(LabDescriptor.C);

            // Act
            var rb = LabDescriptor.A.GetMethod(LabDescriptor.commonMethodName).Invoke(b, new object[] { });
            var rc = LabDescriptor.A.GetMethod(LabDescriptor.commonMethodName).Invoke(c, new object[] { });

            // Assert
            Assert.That(rc, Is.Not.EqualTo(rb));
        }
    }
}
